<?php

/**
 * @file
 * The main file for the module.
 */

/**
 * Hook implementations.
 */

/**
 * Implements hook_init().
 */
function material_scrolltop_init() {
  if (_material_scrolltop_get_variable('enabled')) {
    switch (_material_scrolltop_get_variable('applied_pages')) {
      case 'all':
        _material_scrolltop_add_button();
        break;

      case 'nonadmin_only':
        if (!path_is_admin(current_path())) {
          _material_scrolltop_add_button();
        }
        break;

      case 'admin_only':
        if (path_is_admin(current_path())) {
          _material_scrolltop_add_button();
        }
        break;

      default:
        break;
    }
  }
}

/**
 * Implements hook_menu().
 */
function material_scrolltop_menu() {
  $item = [];

  $items['admin/config/user-interface/material-scrolltop'] = [
    'title' => 'Material ScrollTop Button',
    'description' => 'Configure the settings for Material ScrollTop Button module.',
    'access arguments' => ['administer material scrolltop'],
    'page callback' => 'drupal_get_form',
    'page arguments' => ['material_scrolltop_settings_page'],
    'file' => 'includes/material_scrolltop.admin.inc',
  ];

  return $items;
}

/**
 * Implements hook_permission().
 */
function material_scrolltop_permission() {
  return [
    'administer material scrolltop' =>  [
      'title' => t('Administer Material ScrollTop Button'),
      'description' => t('Configure the settings for Material ScrollTop Button'),
    ],
  ];
}

/**
 * Implements hook_libraries_info().
 */
function material_scrolltop_libraries_info() {
  $libraries['material_scrolltop'] = [
    'name' => 'Material ScrollTop Button',
    'vendor url' => 'http://bartholomej.github.io/material-scrolltop/',
    'download url' => 'https://github.com/bartholomej/material-scrolltop/releases',
    'download file url' => 'https://github.com/bartholomej/material-scrolltop/archive/v1.0.1.tar.gz',
    'path' => 'src',
    'version arguments' => [
      'file' => 'package.json',
      'pattern' => '/"version": "(\d+\.\d+.\d+)"/',
      'lines' => 3,
    ],
    'files' => [
      'js' => [
        'material-scrolltop.js' => ['group' => JS_LIBRARY],
      ],
      'css' => [
        'material-scrolltop.css' => ['group' => CSS_DEFAULT],
      ],
    ],
  ];

  return $libraries;
}

/**
 * Helper functions.
 */

/**
 * Adds the button to page.
 */
function _material_scrolltop_add_button() {
  libraries_load('material_scrolltop');
  drupal_add_js(drupal_get_path('module', 'material_scrolltop') . '/js/material_scrolltop.js');

  if (_material_scrolltop_get_variable('s_enabled')) {
    drupal_add_js([
      'material_scrolltop' => [
        'revealElement' => _material_scrolltop_get_variable('s_revealElement'),
        'revealPosition' => _material_scrolltop_get_variable('s_revealPosition'),
        'padding' => _material_scrolltop_get_variable('s_padding'),
        'duration' => _material_scrolltop_get_variable('s_duration'),
        'easing' => _material_scrolltop_get_variable('s_easing'),
      ],
    ], 'setting');
  }
}

/**
 * Returns the variable value with default value fallback.
 */
function _material_scrolltop_get_variable($raw_name) {
  $prefix = 'material_scrolltop_';
  $name = $prefix . $raw_name;

  switch ($name) {
    case 'material_scrolltop_enabled':
      $value = variable_get($name, FALSE);
      break;

    case 'material_scrolltop_applied_pages':
      $value = variable_get($name, 'all');
      break;

    case 'material_scrolltop_s_enabled':
      $value = variable_get($name, FALSE);
      break;

    case 'material_scrolltop_s_revealElement':
      $value = variable_get($name, 'body');
      break;

    case 'material_scrolltop_s_revealPosition':
      $value = variable_get($name, 'top');
      break;

    case 'material_scrolltop_s_padding':
      $value = variable_get($name, 0);
      break;

    case 'material_scrolltop_s_duration':
      $value = variable_get($name, 600);
      break;

    case 'material_scrolltop_s_easing':
      $value = variable_get($name, 'swing');
      break;

    default:
      throw new MaterialScrolltopException(format_string('Invalid variable name @name is specified.', [
        '@name' => $name,
      ]));
  }

  return $value;

}

/**
 * Custom exception for this module.
 */
class MaterialScrolltopException extends UnexpectedValueException {}
