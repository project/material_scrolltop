(function($) {

Drupal.behaviors.materialScrollTop = {
  attach: function (context, settings) {
    if (context !== document) {
      return;
    }

    var options, $body = $('body');

    if (Drupal.settings.material_scrolltop &&
      typeof(Drupal.settings.material_scrolltop) === 'object') {
      options = Object.assign({}, Drupal.settings.material_scrolltop);
      options.padding = parseInt(options.padding, 10);
      options.duration = parseInt(options.duration, 10);
    }
    else {
      options = {};
    }

    $body.append(
        $('<button>').addClass('material-scrolltop')
      )
      .materialScrollTop(options);
  }
};

})(jQuery);
