<?php

/**
 * @file
 * Contains admin page related functions.
 */

/**
 * Page callback for the module settings page.
 */
function material_scrolltop_settings_page() {
  $form = [];

  $states_common = [
    '#states' => [
      'disabled' => [
        ':input[name="material_scrolltop_s_enabled"]' => ['checked' => FALSE],
      ],
    ],
  ];

  $form['material_scrolltop_enabled'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable Material ScrollTop Button'),
    '#default_value' => _material_scrolltop_get_variable('enabled'),
  ];

  // Applied pages
  $form['material_scrolltop_applied_pages'] = [
    '#type' => 'select',
    '#title' => t('Applied pages'),
    '#options' => [
      'all' => t('All'),
      'nonadmin_only' => t('Nonadmin pages only'),
      'admin_only' => t('Admin pages only'),
    ],
    '#default_value' => _material_scrolltop_get_variable('applied_pages'),
  ];

  $form['material_scrolltop_s_enabled'] = [
    '#type' => 'checkbox',
    '#title' => t('Use the following custom settings'),
    '#default_value' => _material_scrolltop_get_variable('s_enabled'),
  ];

  $url = 'https://github.com/bartholomej/material-scrolltop';
  $form['custom_settings'] = [
    '#type' => 'fieldset',
    '#title' => t('Custom settings'),
    '#description' => t('You can check how the custom settings above work at <a href="@url">the official library page</a>.', [
      '@url' => $url,
    ]),
  ];

  // revealElement
  $form['custom_settings']['material_scrolltop_s_revealElement'] = [
    '#type' => 'textfield',
    // This is better left untranslated.
    '#title' => 'revealElement',
    '#default_value' => _material_scrolltop_get_variable('s_revealElement'),
    '#description' => t('Reveal button when scrolling over specific element (default value: %default)', [
      '%default' => 'body',
    ]),
  ] + $states_common;

  // revealPosition
  $form['custom_settings']['material_scrolltop_s_revealPosition'] = [
    '#type' => 'select',
    // This is better left untranslated.
    '#title' => 'revealPosition',
    '#default_value' => _material_scrolltop_get_variable('s_revealPosition'),
    '#options' => [
      'top' => 'top',
      'bottom' => 'bottom',
    ],
    '#description' => t("Element position for reveal button ('top' or 'bottom') (default value: %default)", [
      '%default' => 'top',
    ]),
  ] + $states_common;

  // duration
  // padding
  $form['custom_settings']['material_scrolltop_s_padding'] = [
    '#type' => 'textfield',
    // This is better left untranslated.
    '#title' => 'padding',
    '#default_value' => _material_scrolltop_get_variable('s_padding'),
    '#description' => t('Adjusts little ups and downs in scrolling (can be negative number) (default value: %default)', [
      '%default' => 0,
    ]),
    '#element_validate' => ['element_validate_integer'],
  ] + $states_common;

  // duration
  $form['custom_settings']['material_scrolltop_s_duration'] = [
    '#type' => 'textfield',
    // This is better left untranslated.
    '#title' => 'duration',
    '#default_value' => _material_scrolltop_get_variable('s_duration'),
    '#description' => t('Determining how long the animation will run (default value: %default)', [
      '%default' => 600,
    ]),
    '#element_validate' => ['element_validate_integer_positive'],
  ] + $states_common;

  // easing
  $form['custom_settings']['material_scrolltop_s_easing'] = [
    '#type' => 'select',
    // This is better left untranslated.
    '#title' => 'easing',
    '#default_value' => _material_scrolltop_get_variable('s_easing', 'swing'),
    '#options' => [
      'swing' => 'swing',
      'linear' => 'linear',
    ],
    '#description' => t('Indicating which easing function to use for the transition animate() (default value: %default)', [
      '%default' => 'swing',
    ]),
  ] + $states_common;

  return system_settings_form($form);
}
