# Material ScrollTop Button module

This module makes it easy to use "Material ScrollTop Button" within Drupal 7 sites.

- https://github.com/bartholomej/material-scrolltop

## Requirements

- PHP 5.6+
- jQuery 1.7+

jQuery plugin "Material ScrollTop Button" depends on jQuery >= 1.7.

Drupal 7 is shipped with jQuery 1.4.4 and you need to replace it with jQuery >= 1.7. You can do that easily by using jQuery update module (https://www.drupal.org/project/jquery_update).

## Necessary modules

- jQuery Update (`jquery_update`)
- Libraries API (`libraries`)

If you use Drush to install Material ScrollTop Button module, these modules are installed automatically.

## Installation

1. Install the necessary modules listed above.
2. Change jQuery version to 1.7+ in your Drupal site.
    - Setting page path: `/admin/config/development/jquery_update`
3. Install Material ScrollTop Button module.
4. Download jQuery plugin Material ScrollTop Button.
    - I recommmend to use Drush to do this step. If you have Drush, you can download it with one command: `drush libraries-download material_scrolltop`.
5. Configure the module settings.
    - setting page path: `/admin/config/user-interface/material-scrolltop`
6. (Optional) Adjust the style of the button.

You must be able to see the button after the step 5.

## Usage

### Behavior options

You can set the values for the options Material ScrollTop Button provides in the setting page: `/admin/config/development/jquery_update`.

You can see the list of available options in the following page.

- https://github.com/bartholomej/material-scrolltop#settings

### Style adjustment

You might want to change the visual style of the scroll-top button.

For example, if you want to change the color and the position of the button, add a scss(css) file like the following.

```scss
.material-scrolltop {
  background-color: blue;
  bottom: 40px;
  right: 40px;

  &:hover {
    background-color: blue;
  }
}
```

## License

Licensed under the GPL2 license. Check https://www.drupal.org/about/licensing for more information on the license.
